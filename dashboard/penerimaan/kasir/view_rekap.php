<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
        header('Access-Control-Allow-Origin: *');
	include $_SERVER['DOCUMENT_ROOT']."/api/setDB02.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	/* getParam **/

	/* database **/
	try {
                $que    = "SELECT a.kar_id,b.kar_nama,b.kp_kode,c.kp_ket,COUNT(*) AS rek_lembar,MAX(a.byr_upd_sts) AS byr_tgl FROM tm_pembayaran a JOIN tm_karyawan b ON(b.kar_id=a.kar_id) JOIN tr_kota_pelayanan c ON(c.kp_kode=b.kp_kode) WHERE DATE_FORMAT(a.byr_tgl,'%Y%m')=DATE_FORMAT(NOW(),'%Y%m') GROUP BY a.kar_id";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
