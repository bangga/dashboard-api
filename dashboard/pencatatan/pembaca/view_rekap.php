<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
    header('Access-Control-Allow-Origin: *');
	include $_SERVER['DOCUMENT_ROOT']."/api/setDB03.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	/* getParam **/

	/* database **/
	try {
        $que	= "SELECT IFNULL(b.kar_id,'catermkm') AS kar_id,IFNULL(b.kar_nama,'Cater MKM') AS kar_nama,IFNULL(c.kp_kode,'00') AS kp_kode,IFNULL(c.kp_ket,'Cater MKM') AS kp_nama,SUM(IF(ISNULL(a.petugas),0,1)) AS reff,e.sl_jml AS reff_kini,e.sm_jml AS reff_lalu,SUM(IF(ISNULL(a.petugas),0,1)) AS sl_dibaca,REPLACE(d.latitude,',','.') AS K,REPLACE(d.longitude,',','.') AS A,IFNULL(b.kar_nama,d.petugas) AS O,MAX(STR_TO_DATE(CONCAT(d.tglcatat,' ',d.jamcatat),'%e/%c/%Y %k:%i')) AS sm_tgl FROM petugasmeter_akhir d LEFT JOIN db_rekap_bacaan e ON(e.kar_id=d.petugas) LEFT JOIN catatmeter a ON(a.petugas=d.petugas AND a.blncatat=DATE_FORMAT(NOW(),'%Y%m')) LEFT JOIN tm_karyawan b ON(b.kar_id=d.petugas) LEFT JOIN tr_kota_pelayanan c ON(c.kp_kode=b.kp_kode) GROUP BY IF(ISNULL(b.kar_id),1,b.kar_id)";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
