<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
    header('Access-Control-Allow-Origin: *');
	include $_SERVER['DOCUMENT_ROOT']."/api/setDB02.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	/* getParam **/

	/* database **/
	try {
        $que    = "SELECT spt_dicetak,rek_dibayar_spt,penutupan,CONCAT(SUBSTR(periode,1,4),'-',SUBSTR(periode,5,2)) AS periode FROM temp_gart.db_efesiensi_spt WHERE PERIOD_DIFF(DATE_FORMAT(NOW(),'%Y%m'),periode)<30";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
