<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
        header('Access-Control-Allow-Origin: *');
	include $_SERVER['DOCUMENT_ROOT']."/api/setDB01.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	/* getParam **/

	/* database **/
	try {
		// $que 	= "SELECT a.unit,COUNT(*) AS sl_diorder,SUM(IF(IFNULL(b.settlement,2)<2,1,0)) AS sl_dibaca,SUM(IF(IFNULL(b.settlement,0)=1,1,0)) AS sl_disettle,SUM(IF(ISNULL(b.standmeter),1,0)) AS sl_belum_dibaca FROM masterpelanggan a LEFT JOIN catatmeter b ON(b.no_sl=a.no_sl AND b.blncatat=a.periodedata) WHERE a.periodedata=DATE_FORMAT(NOW(),'%Y%m') GROUP BY a.kode_unit";
                $que    = "SELECT c.kp_ket AS unit,COUNT(*) AS sl_diorder,SUM(IF(IFNULL(b.settlement,2)<2,1,0)) AS sl_dibaca,SUM(IF(IFNULL(b.settlement,0)=1,1,0)) AS sl_disettle,SUM(IF(ISNULL(b.standmeter),1,0)) AS sl_belum_dibaca FROM tm_pelanggan a JOIN tr_kota_pelayanan c ON(c.kp_kode=a.kp_kode) LEFT JOIN catatmeter b ON(b.no_sl=a.pel_no AND b.blncatat=DATE_FORMAT(NOW(),'%Y%m')) WHERE a.kps_kode<7 GROUP BY a.kp_kode";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
        flush();
?>

