<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tirtaintan</title>

    <!-- Bootstrap Core CSS -->
    <link href="http://simeut.tirtaintan.co.id/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="http://simeut.tirtaintan.co.id/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="http://simeut.tirtaintan.co.id/css/plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="http://simeut.tirtaintan.co.id/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <!--<link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Order Survey</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row" id="page-content">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Upload Data</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-4">
<?php
	include 'setDB06.php';
	include 'logging.php';
	$log    = new errorLog();
	session_start();

	define('_KODE', '000000');
	define('_HOST', $_SERVER['REMOTE_ADDR']);
	define('_TOKN', uniqid());

	$token 	= "";
	$errno	= 1;
	if(isset($_POST['token'])){
		$token 	= $_POST['token'];
		$errno	= cek_login($PLINK,$log,$token);
	}

	if($errno==0){
		$kps_kode	= 0;
		$order_kode	= 0;
		if(isset($_POST['kps_kode'])){
			$kps_kode	= $_POST['kps_kode'];
			if(isset($_POST['order_kode'])){
				$order_kode	= $_POST['order_kode'];
			}
		}
		if($kps_kode>0){

			if(isset($_FILES['image']['tmp_name'])){
				if($_FILES['image']['error']==0){
					move_uploaded_file($_FILES['image']['tmp_name'], '/home/tirtaintan/gambar/'._TOKN.'.jpg');
					$file_gambar	= _TOKN.".jpg";
				}
				else{
					$pesan 			= "File gagal diupload";
					$file_gambar	= "Untitled.jpg";
					$log->logMess($pesan);
				}
			}
			else{
				$pesan 			= "File tidak diupload";
				$file_gambar	= "Untitled.jpg";
				$log->logMess($pesan);
			}
			
			try{
				$PLINK->beginTransaction();

				// create audit trail
				$que   	= "INSERT pdam_gart.tr_trans_log(tr_tgl,tr_id,tr_sts,tr_ip,kp_kode,kar_id) VALUES(NOW(),'"._TOKN."',5,INET_ATON('"._HOST."'),'"._KOTA."','"._USER."')";
				$PLINK->exec($que);
				$log->logDB($que);

				// update parameter dasar
				$que    = "UPDATE tm_order a JOIN tm_user_akses b ON(b.kar_id='"._USER."') SET a.result_id='"._TOKN."',a.survey_tgl=NOW(),a.latkor=b.latkor,a.lonkor=b.lonkor WHERE a.order_id='".$_POST['order_id']."'";
				$PLINK->exec($que);
				$log->logDB($que);
				
				// update proses bp && pk untuk proses survey
				$que    = "UPDATE tm_order a JOIN pdam_gart.tm_pemohon_bp c ON(c.pem_reg=a.client_id) SET c.pem_ref_pasang=CASE a.order_kode WHEN 2 THEN '"._TOKN."' ELSE c.pem_ref_pasang END,c.pem_sts=CASE a.order_kode WHEN 1 THEN ".$kps_kode." WHEN 2 THEN 16 ELSE 17 END WHERE order_id='".$_POST['order_id']."' AND ISNULL(c.pem_tgl_aktif)";
				$PLINK->exec($que);
				$log->logDB($que);

				// update proses penutupan && meterisasi
				$que    = "UPDATE tm_order a JOIN pdam_gart.tm_pelanggan c ON(c.pel_no=a.client_id) SET c.kps_kode=CASE WHEN (c.kps_kode=5 AND a.order_kode=4) THEN 18 WHEN (c.kps_kode=4 AND a.order_kode=2) THEN 20 WHEN (c.kps_kode=20 AND a.order_kode=3) THEN 21 ELSE c.kps_kode END WHERE a.order_id='".$_POST['order_id']."'";
				$PLINK->exec($que);
				$log->logDB($que);

				// insert common result
				$que	= "INSERT INTO tm_result(result_id,result_kode,result_value) VALUES('"._TOKN."','4','".$file_gambar."')";
				$PLINK->exec($que);
				$log->logDB($que);

				$kunci	= array_keys($_POST);
				for($i=0;$i<count($_POST);$i++){
					switch($kunci[$i]){
						case 'hub_pel_no':
							$result_kode = 1;
							break;
						case 'gol_kode':
							$result_kode = 2;
							break;
						case 'catatan':
							$result_kode = 3;
							break;
						case 'wm_no':
							$result_kode = 5;
							break;
						case 'um_kode':
							$result_kode = 6;
							break;
						case 'merk_kode':
							$result_kode = 7;
							break;
						case 'stand_pasang':
							$result_kode = 8;
							break;
						case 'kps_kode':
							$result_kode = 9;
							break;
						case 'stand_angkat':
							$result_kode = 10;
							break;
						case 'kondisi_wm':
							$result_kode = 11;
							break;
						case 'kondisi_lk':
							$result_kode = 12;
							break;
						case 'sm_kini':
							$result_kode = 13;
							break;
						default:
							$result_kode = 0;
					}

					if($result_kode>0){
						// insert specific result
						$que	= "INSERT INTO tm_result(result_id,result_kode,result_value) VALUES('"._TOKN."','".$result_kode."','".$_POST[$kunci[$i]]."')";
						$PLINK->exec($que);
						$log->logDB($que);
					}
				}
				$PLINK->commit();
				$pesan  = "Perubahan telah disimpan";
				unset($PLINK);
			}
			catch (PDOException $e){
				$pesan  = "Mungkin terjadi kesalahan pada koneksi database";
				$log->errorDB($e->getMessage());
				$log->logDB($que);
			}

			switch ($order_kode){
				case 7:
					$targetUrl	= "http://simeut.tirtaintan.co.id/cater.html";
?>
	<script>
		var dataTemp 	= JSON.parse(localStorage.order);
		var dataProc	= dataTemp[dataTemp.procKode];
		var procKode	= dataProc.procKode;
		procKode++;
		dataTemp[dataTemp.procKode].procKode = procKode;
		localStorage.setItem('order', JSON.stringify(dataTemp));
	</script>
<?php
					break;
				default:
					$targetUrl	= "http://simeut.tirtaintan.co.id/order/diorder";
			}			
		}
		else{
			$pesan 		= "Hasil survey belum dipilih";
			
			switch ($order_kode){
				case 1:
					$targetUrl	= "http://simeut.tirtaintan.co.id/survey.html";
					break;
				case 2:
					$targetUrl	= "http://simeut.tirtaintan.co.id/gudang.html";
					break;
				case 3:
					$targetUrl	= "http://simeut.tirtaintan.co.id/hublang.html";
					break;
				case 4:
					$targetUrl	= "http://simeut.tirtaintan.co.id/bongkar.html";
					break;
				case 7:
					$targetUrl	= "http://simeut.tirtaintan.co.id/cater.html";
					break;
				default:
					$targetUrl	= "http://simeut.tirtaintan.co.id/blank.html";
			}
		}
	}
	else{
		$pesan 		= "Sesi tidak terdaftar";
		$targetUrl	= "http://simeut.tirtaintan.co.id";
		define('_USER', '000000');
	}
	$log->logMess($pesan);
?>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4">
									<div class="alert alert-success"><?php echo $pesan; ?></div>
									<a href="<?php echo $targetUrl; ?>" class="btn btn-success">Kembali</a>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery Version 1.11.0 -->
    <script src="http://simeut.tirtaintan.co.id/js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="http://simeut.tirtaintan.co.id/js/bootstrap.min.js"></script>
</body>

</html>
