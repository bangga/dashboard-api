<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://simeut.tirtaintan.co.id');
	header('Access-Control-Allow-Credentials: true');

	include "../../../setDB01.php";
	include "../../../logging.php";
	$log    = new errorLog();
	
	$pilihan = "";
	if(isset($_GET['pilihan'])){
		$pilihan = $_GET['pilihan'];
	}

	define('_KODE', '000000');
	define('_USER', 'jerbee');
	define('_GRUP', '000');
	define('_KOTA', '00');
	define('_HOST', $_SERVER['REMOTE_ADDR']);
	define('_TOKN', uniqid());

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		switch($pilihan){
			default:
				if(_GRUP=='000'){
					$filter	= "WHERE result_sts=1";
				}
				else{
					$filter	= "WHERE kar_id='"._USER."' AND result_sts=1";
				}
				$limit = "";
				if(isset($_POST['draw'])){
					if(strlen($_POST['search']['value'])>=3){
						$filter .= " AND (client_id='".$_POST['search']['value']."' OR UPPER(client_nama) LIKE '%".$_POST['search']['value']."%')";
					}
					$limit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
		}
	}
	/* getParam **/

	/* database **/
	try {
		$recordsFiltered	= 0;

		$que    = "SELECT a.dkd_kd AS dkd_kd,a.dkd_jalan AS dkd_jalan,a.kar_id AS kar_id,a.dkd_tcatat AS dkd_tcatat,COUNT(b.pel_no) AS sl_jml,CONCAT('<span style=\"cursor: pointer\" onclick=\"loadFile(\'',a.dkd_kd,'\')\">',a.dkd_jalan,'</span>') AS dkd_ket FROM (SELECT dkd_kd,dkd_jalan,kar_id,dkd_tcatat FROM pdam_gart.tr_dkd WHERE kar_id='"._USER."' LIMIT 1,1000) a JOIN pdam_gart.tm_pelanggan b ON(b.dkd_kd=a.dkd_kd) WHERE pdam_gart.isActive(b.kps_kode) GROUP BY a.dkd_kd";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$recordsTotal	= count($row);
		unset($PLINK);
	}
	catch (PDOException $e){
		$error	= $e->getMessage();
		$errno	= 1;
		$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$error, "errno"=>$errno);
		$log->errorDB($error);
	}

	echo "{\"draw\": \"".$_POST['draw']."\", \"recordsTotal\": \"".$recordsTotal."\", \"recordsFiltered\": \"".$recordsFiltered."\", \"data\": ".json_encode($row)."}";
    flush();
?>
