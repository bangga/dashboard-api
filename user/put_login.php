<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://simeut.tirtaintan.co.id');

	include "../setDB06.php";
	include "../logging.php";
	$log    = new errorLog();

	/** getParam
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai  = array();
	if(isset($_POST['data'])){
		$nilai = $_POST['data'];
	}
	for($i=0;$i<count($nilai);$i++){
		$$nilai[$i]['name'] = $nilai[$i]['value'];
	}
	/* getParam **/

	$count		= 0;
	$row		= array();
	$kar_pass	= md5($kar_pass);
	define("_KODE", "000000");
	define("_USER", $kar_id);
	define("_HOST", $_SERVER['REMOTE_ADDR']);
	define('_TOKN', uniqid());

	/* database **/
	try {
		$PLINK->beginTransaction();
		$que        = "SELECT kar_id,kar_nama,grup_id,kp_kode FROM pdam_gart.tm_karyawan WHERE kar_id='".$kar_id."' AND kar_pass='".$kar_pass."'";
		$sth        = $PLINK->prepare($que);
		$sth->execute();
		if($row = $sth->fetch(PDO::FETCH_ASSOC)){
			$que	= "INSERT pdam_gart.tr_trans_log(tr_tgl,tr_id,tr_sts,tr_ip,kp_kode,kar_id) VALUES(NOW(),'"._TOKN."',1,INET_ATON('"._HOST."'),'".$row['kp_kode']."','".$kar_id."')";
			$count	= $PLINK->exec($que);
			if($count>0){
				$PLINK->commit();
				$log->logDB($que);
				$pesan = "Login berhasil dilakukan";
				$errno = 0;						
			}
			else{
				$pesan = "Login tidak bisa dilakukan";
				$errno = 1;
			}
		}
		else{
			$pesan  = "Login gagal dilakukan";
			$errno  = 2;
		}
		$row['token']	= _TOKN;
		$row            = array("pesan"=>$pesan, "errno"=>$errno, "data"=>$row);
		unset($PLINK);
	}
	catch (PDOException $e){
		$PLINK->rollBack();
		$pesan  = "Mungkin terjadi kesalahan pada koneksi database";
		$error  = $e->getMessage();
		$errno  = 3;
		$row    = array("pesan"=>$pesan, "error"=>$error, "errno"=>$errno);
		$log->errorDB($error);
	}

	echo json_encode($row);
	$log->logMess($pesan);
    flush();
?>
