<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://simeut.tirtaintan.co.id');

	include "../setDB06.php";
	include "../logging.php";
	$log    = new errorLog();

	define('_KODE', '000000');
	define('_HOST', $_SERVER['REMOTE_ADDR']);
	define('_TOKN', uniqid());

	$token	= $_GET['token'];
	$kar_id	= "000000";
	$pesan  = "Sesi telah berakhir";
	$count	= 0;
	$errno 	= 1;
	$defin	= 0;

	try {
		$que   	= "SELECT tr_id,kar_id FROM pdam_gart.last_trans_log WHERE tr_id='".$token."' AND tr_sts=1 AND DATE(tr_tgl)=CURDATE() AND MINUTE(TIMEDIFF(NOW(),tr_tgl))<10";
		foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
			$kar_id = $row['kar_id'];
			if($defin==0){
				define('_USER', $kar_id);
				$defin	= 1;
			}
			$PLINK->beginTransaction();
			$que	= "UPDATE pdam_gart.last_trans_log SET tr_tgl=NOW() WHERE tr_id='".$token."'";
			$count	= $PLINK->exec($que);
			if($count>0){
				$PLINK->commit();
				$log->logDB($que);
				$pesan = "Sesi telah diperpanjang";
				$errno = 0;
			}
			else{
				$pesan = "Sesi tidak diperpanjang";
				$errno = 1;
			}
		}
		if($defin==0){
			define('_USER', $kar_id);
			$defin	= 1;
		}
	}
	catch (PDOException $e){
		$pesan  = "Mungkin terjadi kesalahan pada koneksi database";
		$error  = $e->getMessage();
		$errno  = 3;
		if($defin==0){
			define('_USER', $kar_id);
			$defin	= 1;
		}
		$log->errorDB($error);
		$log->logDB($que);
	}

	if(isset($error)){
		$row    = array('pesan'=>$pesan, 'error'=>$error, 'errno'=>$errno);
	}
	else{
		$row    = array('pesan'=>$pesan, 'errno'=>$errno);
	}

	echo json_encode($row);
	$log->logMess($pesan);
    flush();
?>
