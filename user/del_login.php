<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://simeut.tirtaintan.co.id');

	include "../setDB06.php";
	include "../logging.php";
	$log    = new errorLog();

	$row	= array();
	$pesan	= "";
	define("_KODE", "000000");
	define("_HOST", $_SERVER['REMOTE_ADDR']);
	define('_TOKN', uniqid());
	
	if(isset($_GET['token'])){
		$token 	= $_GET['token'];
		$errno	= cek_login($PLINK,$log,$token);
	}

	if($errno==0){
		/* database **/
		try {
			$PLINK->beginTransaction();
			$que	= "INSERT pdam_gart.tr_trans_log(tr_tgl,tr_id,tr_sts,tr_ip,kp_kode,kar_id) VALUES(NOW(),'"._TOKN."',2,INET_ATON('"._HOST."'),'"._KOTA."','"._USER."')";
			$count	= $PLINK->exec($que);
			if($count>0){
				$PLINK->commit();
				$log->logDB($que);
				$pesan = "Logout berhasil dilakukan";
				$errno = 0;						
			}
			else{
				$pesan = "Logout tidak bisa dilakukan";
				$errno = 1;
			}

			$row['token']	= _TOKN;
			$row            = array("pesan"=>$pesan, "errno"=>$errno, "data"=>$row);
			unset($PLINK);
		}
		catch (PDOException $e){
			$PLINK->rollBack();
			$pesan  = "Mungkin terjadi kesalahan pada koneksi database";
			$error  = $e->getMessage();
			$errno  = 3;
			$row    = array("pesan"=>$pesan, "error"=>$error, "errno"=>$errno);
			$log->logDB($que);
			$log->errorDB($error);
		}
	}
	else{
		$row    = array("pesan"=>"Sesi telah berakhir", "errno"=>0);
	}

	echo json_encode($row);
	$log->logMess($pesan);
    flush();
?>
