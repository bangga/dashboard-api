<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://www.tirtaintan.co.id');

	include "../../setDB02.php";
	include "../../logging.php";
	$log    = new errorLog();
	
	$pilihan = "";
	if(isset($_GET['pilihan'])){
		$pilihan = $_GET['pilihan'];
	}

	define('_KODE', '000000');
	define('_USER', '000000');
	define('_HOST', $_SERVER['REMOTE_ADDR']);
	define('_TOKN', uniqid());

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		switch($pilihan){
			default:
				if(_GRUP=='000'){
					$filter	= "WHERE result_sts=1";
				}
				else{
					$filter	= "WHERE kp_kode='"._KOTA."' AND result_sts=1";
				}
				$limit = "LIMIT 0";
				if(isset($_POST['draw'])){
					if(strlen($_POST['search']['value'])>=3){
						$filter .= " AND (client_id='".$_POST['search']['value']."' OR UPPER(client_nama) LIKE '%".$_POST['search']['value']."%')";
					}
					$limit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
		}
	}
	/* getParam **/

	$recordsFiltered	= 0;
	$recordsTotal		= 0;
	$draw				= 0;
	if(isset($_POST['draw'])){
		$draw = $_POST['draw'];
	}
	/* database **/
	try {
		$que    = "SELECT a.pel_no AS client_id,a.pel_nama AS client_nama,a.pel_alamat AS client_alamat,b.gol_kel AS client_grup FROM pdam_gart.tm_pelanggan a JOIN pdam_gart.tr_gol b ON(b.gol_kode=a.gol_kode) WHERE a.pel_no='".$_GET['pel_no']."'";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$recordsTotal	= count($row);
		unset($PLINK);
	}
	catch (PDOException $e){
		$error	= $e->getMessage();
		$errno	= 1;
		$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$error, "errno"=>$errno);
		$log->errorDB($error);
	}

	echo "{\"draw\": \"".$draw."\", \"recordsTotal\": \"".$recordsTotal."\", \"recordsFiltered\": \"".$recordsFiltered."\", \"data\": ".json_encode($row)."}";
    flush();
?>
