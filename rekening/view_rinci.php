<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include "../setDB02.php";
	include "../logging.php";
	$log    = new errorLog();
	
	$pilihan = "";
	if(isset($_GET['pilihan'])){
		$pilihan = $_GET['pilihan'];
	}

	define('_KODE', '000000');
	define('_USER', '000000');
	define('_HOST', $_SERVER['REMOTE_ADDR']);
	define('_TOKN', uniqid());

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
			if(isset($_GET['draw'])){
				$limit = " LIMIT ".$_GET['start'].",".$_GET['length'];
			}
		}
		else{
			$filter	= "WHERE ISNULL(pel_no)";
			$limit	= "";
		}
	}
	else{
                $filter = "WHERE pel_no='".$_GET['data']."'";
                $draw   = 0;
                if(isset($_GET['draw'])){
                        if(strlen($_GET['draw'])>0){
                               $limit = "LIMIT ".$_GET['start'].",".$_GET['length'];
                               $draw  = $_GET['draw'];
                        }
                }
	}
	/* getParam **/

	/* database **/
	try {
		$que    = "SELECT COUNT(*) AS reff FROM v_tagihan ".$filter;
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row				= $sth->fetch(PDO::FETCH_ASSOC);
		$recordsFiltered	= $row['reff'];

		$que    = "SELECT * FROM v_tagihan ".$filter." ".$limit;
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$recordsTotal	= count($row);
		unset($PLINK);
	}
	catch (PDOException $e){
		$error	= $e->getMessage();
		$errno	= 1;
		$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$error, "errno"=>$errno);
		$log->errorDB($error);
                $log->logDB($que);
	}
	echo "{\"draw\": \"".$draw."\", \"recordsTotal\": \"".$recordsTotal."\", \"recordsFiltered\": \"".$recordsFiltered."\", \"data\": ".json_encode($row)."}";
    flush();
?>
