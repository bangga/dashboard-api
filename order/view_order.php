<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://simeut.tirtaintan.co.id');

	define('_KODE', '000000');
	define('_HOST', $_SERVER['REMOTE_ADDR']);
	define('_TOKN', uniqid());
	
	include "../setDB06.php";
	include "../logging.php";
	$log    = new errorLog();
	
	$pilihan = "";
	if(isset($_GET['pilihan'])){
		$pilihan = $_GET['pilihan'];
	}
	
	$token 	= "";
	$errno	= 1;
	$recordsTotal		= 0;
	$recordsFiltered	= 0;
	if(isset($_GET['token'])){
		$token 	= $_GET['token'];
		$errno	= cek_login($PLINK,$log,$token);
	}

	if($errno==0){
		/** getParam 
			memindahkan semua nilai dalam array POST ke dalam
			variabel yang bersesuaian dengan masih kunci array
		*/
		$order	= "";
		$limit  = " LIMIT 0";
		if(isset($_POST['filter'])){
			$filter	= array();
			$nilai	= $_POST['filter'];
			for($i=0;$i<count($nilai);$i++){
				$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
			}
			if($i>0){
				$filter	= "WHERE ".implode(' AND ',$filter);
			}
		}
		else{
			switch($pilihan){
				case 'diorder':
					if(_GRUP=='000'){
						$filter	= "WHERE order_sts=1 AND order_kode<7 AND result_sts=0";
					}
					else if(_KOTA=='00'){
						$filter	= "WHERE order_sts=1 AND order_kode<7 AND result_sts=0";
					}
					else{
						$filter	= "WHERE kp_kode='"._KOTA."' AND order_sts=1 AND order_kode<7 AND result_sts=0";						
					}
					if(isset($_POST['draw'])){
						if(strlen($_POST['search']['value'])==6){
							$filter .= " AND client_id='".$_POST['search']['value']."'";
						}
						else if(strlen($_POST['search']['value'])>0){
							$filter .= " AND SUBSTR(client_id,1,".strlen($_POST['search']['value']).")='".$_POST['search']['value']."'";
						}
						$limit = " LIMIT ".$_POST['start'].",".$_POST['length'];
					}
					break;
				case 'disurvey':
					if(_GRUP=='000'){
						$filter	= "WHERE result_sts=1";
					}
					else{
						$filter	= "WHERE kar_id='"._USER."' AND result_sts=1";
					}
					if(isset($_POST['draw'])){
						if(strlen($_POST['search']['value'])==6){
							$filter .= " AND client_id='".$_POST['search']['value']."'";
						}
						else if(strlen($_POST['search']['value'])>0){
							$filter .= " AND SUBSTR(client_id,1,".strlen($_POST['search']['value']).")='".$_POST['search']['value']."'";
						}
						$limit = " LIMIT ".$_POST['start'].",".$_POST['length'];
					}
					break;
				default:
					$filter	= "LIMIT 0";
			}
			if(isset($_POST['order'])){
				if($_POST['order'][0]['column']>0){
					$index 	= $_POST['order'][0]['column'];
					$column = $_POST['columns'][$index]['data'];
					$sort	= $_POST['order'][0]['dir'];
					$order	= "ORDER BY ".$column." ".$sort;
				}
			}
		}
		/* getParam **/

		/* database **/
		try {
			$que    = "SELECT COUNT(client_id) AS reff FROM caterpdam.tm_order WHERE order_sts=1 AND ISNULL(result_id)";
			$sth 	= $PLINK->prepare($que);
			$sth->execute();
			$row				= $sth->fetch(PDO::FETCH_ASSOC);
			$recordsFiltered	= $row['reff'];

			$que    = "SELECT * FROM caterpdam.v_order ".$filter." ".$order." ".$limit;
			$sth 	= $PLINK->prepare($que);
			$sth->execute();
			$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
			$recordsTotal	= count($row);
			unset($PLINK);
		}
		catch (PDOException $e){
			$error	= $e->getMessage();
			$errno	= 1;
			$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$error, "errno"=>$errno);
			$log->errorDB($error);
		}
	}
	else{
		$row = array("pesan"=>"Sesi tidak terdaftar","errno"=>$errno);
	}
	
	echo "{\"draw\": \"".$_POST['draw']."\", \"recordsTotal\": \"".$recordsTotal."\", \"recordsFiltered\": \"".$recordsFiltered."\", \"data\": ".json_encode($row)."}";
    flush();
?>
