<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://simeut.tirtaintan.co.id');
	header('Access-Control-Allow-Credentials: true');

	include "../setDB06.php";
	include "../logging.php";
	$log    = new errorLog();
	session_start();
	
	$pilihan = "";
	if(isset($_GET['pilihan'])){
		$pilihan = $_GET['pilihan'];
	}

	define('_KODE', '000000');
	define('_USER', '000000');
	define('_HOST', $_SERVER['REMOTE_ADDR']);
	define('_TOKN', uniqid());

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		if(strlen($pilihan)==6){
			$filter	= "WHERE client_id='".$pilihan."'";
		}
		else{
			$filter	= "WHERE result_id='".$pilihan."'";
		}
	}
	/* getParam **/

	/* database **/
	try {
		$que    = "SELECT * FROM caterpdam.v_result ".$filter;
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
		$error	= $e->getMessage();
		$errno	= 1;
		$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$error, "errno"=>$errno);
		$log->errorDB($error);
	}

	echo "{\"data\": ".json_encode($row)."}";
    flush();
?>

