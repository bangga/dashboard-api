<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://sopp-bsm.tirtaintan.co.id');

	// create a new cURL resource
	$ch = curl_init();

	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, 'http://core-data.tirtaintan.co.id/enquiry/loketbsm/'.$_GET['data']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$hasil = json_decode(curl_exec($ch));
	
	// close cURL resource, and free up system resources
	curl_close($ch);
	
	if($hasil->recordsTotal<2){
		echo json_encode($hasil);
	}
	else{
		$token		= $hasil->token;
		unset($hasil);
		$hasil['token']	= $token;
		$hasil['errno']	= 5;
		$hasil['error']	= "Rekening harus dibayar di loket PDAM";
		// {"token": "57fc7b7762098", "errno": "5", "error": "rekening harus dibayar di loket PDAM"}
		echo json_encode($hasil);
	}
	
	flush();
?>
