<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');
	
	include $_SERVER['DOCUMENT_ROOT']."/setDB01.php";
	include $_SERVER['DOCUMENT_ROOT']."/logging.php";
	
	$log    = new errorLog();

	/** getParam
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	foreach(json_decode(hex2bin($_GET['data'])) as $key => $value){
		$$key = $value;
	}
	/* getParam **/

	$count	= 0;
	$hasil	= new stdClass();
	define('_KODE', 'loketbsm');
	define('_USER', 'loketbsm');
	define('_HOST', $_SERVER['REMOTE_ADDR']);
	define('_TOKN', uniqid());

	/* database **/
	try {
		$PLINK->beginTransaction();
		
		for($i=0;$i<count($data);$i++){
			foreach($data[$i] as $key => $value) {
				$$key = $value;
			}
			$que	= "UPDATE pdam_gart.tm_rekening SET rek_byr_sts=1 WHERE rek_nomor=".$rek_nomor." AND rek_sts=1 AND rek_byr_sts=0 AND (rek_total+rek_denda)=".$rek_total;
			if($PLINK->exec($que)>0){
				$log->logDB($que);
				$count++;
			}
			$que	= "INSERT INTO pdam_gart.tm_pembayaran(byr_no,byr_tgl,byr_serial,rek_nomor,kar_id,lok_ip,byr_loket,byr_total,byr_cetak,byr_upd_sts,byr_sts) VALUES('".$token."',STR_TO_DATE(".$byr_tgl.",'%Y%m%d%H%i%s'),'".$serial."',".$rek_nomor.",'"._USER."','".$_SERVER['REMOTE_ADDR']."','".$loket."',".$rek_total.",1,NOW(),1)";
			if($PLINK->exec($que)>0){
				$log->logDB($que);
				$count++;
			}
		}
		
		if($count==(2*count($data))){			
			if(_KODE=='devel'){
				$hasil->errno=0;
			}
			else{
				// update transaksi ke core billing
				$ch = curl_init();

				// set URL and other appropriate options
				curl_setopt($ch, CURLOPT_URL, 'http://core-data.tirtaintan.co.id/payment/'._USER.'/'.$_GET['data']);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				$hasil	= json_decode(curl_exec($ch));
			}
			
			if(isset($hasil->errno) && $hasil->errno==0){
				if(_KODE=='devel'){
					$PLINK->rollBack();
				}
				else{
					$PLINK->commit();
				}
				$errno = 0;
				$error = count($data)." transaksi berhasil berhasil dilakukan";
			}
			else{
				$PLINK->rollBack();
				$errno = 2;
				$error = "Sesi telah berakhir, silahkan periksa info tagihan kembali";
			}
			// close cURL resource, and free up system resources
			curl_close($ch);

		}
		else{
			$PLINK->rollBack();
			$errno = 2;
			$error = "Sesi telah berakhir, silahkan periksa info tagihan kembali";
		}
		unset($PLINK);
	}
	catch (PDOException $e){
		$PLINK->rollBack();
		$log->errorDB($e->getMessage());
		$errno  = 1;
		$error  = "Terjadi gangguan teknis, cobalah beberapa saat lagi";
	}

	$log->logMess($error);
	echo "{\"token\": \"".$token."\", \"errno\": \"".$errno."\", \"error\": \"".$error."\"}";
	flush();
?>
