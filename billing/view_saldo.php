<?php
        header('Content-Type: application/json');
        header('Cache-Control: no-cache');
        header('Access-Control-Allow-Origin: sopp-arindo.tirtaintan.co.id');

        // create a new cURL resource
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, 'http://core-data.tirtaintan.co.id/enquiry/arindo/'.$_GET['data']);
        curl_setopt($ch, CURLOPT_PORT, 80);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_SERVER));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Real-IP: '.$_SERVER['REMOTE_ADDR']));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        echo curl_exec($ch);

        // close cURL resource, and free up system resources
        curl_close($ch);

        flush();

