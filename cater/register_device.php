<?php
/*
	Change Log
	# Migrasi server kerinci ke vps [06-04-2016]
		- Penyederhanaan query transaksi
		- Logging diarahkan ke sqlite
		- Penyederhanaan proses transaksi ke database, hanya menggunakan single insert query
		- CALL API : /mmrtsi/api/register_device.php?f_per_nama=sakti1&f_per_mac=38:D4:0B:19:48:19&f_per_no=8962101074131002657&f_per_imei=358542061304167&f_out_id=tirtasakti
	# Release
*/

	header("Content-Type: text/json; charset=UTF8");

/** getParam
    memindahkan semua nilai dalam array GET ke dalam variabel yang bersesuaian dengan masih kunci array
*/
	$nilai = $_GET;
	$konci = array_keys($nilai);
	for($i=0;$i<count($konci);$i++){
		$$konci[$i] = $nilai[$konci[$i]];
	}
/*  getParam
**/

	define('_USER','register');
	define('_KODE','register');
	define('_TOKN', uniqid());
	define('_HOST',$_SERVER['REMOTE_ADDR']);

	require('../logging.php');
	require('../setDB01.php');
	$log    = new errorLog();

	$stat_entry = true;
	$sts_valid 	= false;
	$msg 		= "";
	$f_per_id	= "";
		 
	if($f_per_id == ""){
		if(($f_per_imei <> "") AND $f_per_no <>"" ){
			$per_id 	= md5($f_per_no.$f_per_imei);
			$sts_valid 	= true;
		}
	}
	else{
		$sts_valid = true;
	}
	
    if((empty($f_per_no)) && (empty($f_per_imei)) && (empty($f_per_mac)) && (empty($f_per_nama))) {
        $msg = "\nPeringatan:  \ndata yang terkirim salah/kurang lengkap,\npengiriman dibatalkan!";
		echo json_encode(array('send_respon' => $msg));
    }
	else{
		try{
			$PLINK->beginTransaction();
			$que = "INSERT INTO caterpdam.tm_perangkat(per_id,per_nama,per_no,per_imei,per_mac,per_entry,per_sts,out_id) VALUES('$per_id','$f_per_nama','$f_per_no','$f_per_imei','$f_per_mac',NOW(),'1','$f_out_id') ON DUPLICATE KEY UPDATE per_entry=NOW()";
			$count	= $PLINK->exec($que);
			$msg[] 	= array(
				'Pesan'  => "Data Tersimpan" ,
				'IDPerangkat'=>$per_id,
				'NamaPerangkat'=>$f_per_nama 
			);
			$log->logMess('Registrasi berhasil');
			$log->logDB($que);
			$PLINK->commit();
			echo json_encode(array('send_respon' => $msg));
		}
		catch (Exception $e){
			$log->logMess('Terjadi kesalahan pada basis data');
			$log->errorDB($e->getMessage());
			$log->logDB($que);
			header("HTTP/1.1 500 Internal Server Error");
			echo json_encode(array('reference_area'=>$e->getMessage()));
		}
	}
	
	flush();
?>
