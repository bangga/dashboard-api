<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');
	
	// inquiri data bacaan terakhir
	require "setDB.php";
	try{
		$que 			= "SELECT a.wmmr_tgl_baca AS D,a.lonkor AS A,a.latkor AS K,b.out_id AS O FROM tm_wmmr_sm a JOIN tm_karyawan b ON(b.kar_id=a.kar_id) ORDER BY a.wmmr_tgl_baca DESC LIMIT 5";
		$sth 			= $link->prepare($que);
		$sth->execute();
		$data_val		= $sth->fetchAll(PDO::FETCH_ASSOC);
		$link			= null;
	}
	catch(Exception $e){
		//$log->errorDB($e->getMessage());
		//$log->logMess("Gagal melakukan inquiri data rekening");
		//$log->logDB($que);
	}

	
	$rand_key 	= array_rand($data_val, 5);
	for($i=0;$i<5;$i++){
		$data_str[] = json_encode($data_val[$rand_key[$i]]);
	}
	
	echo "data: [".implode(",",$data_str)."]\n\n";
	flush();
?>