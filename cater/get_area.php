<?php

/*
	Change Log
	# Migrasi server kerinci ke vps [30-03-2016]
		- Penyederhanaan query transaksi
		- Logging diarahkan ke sqlite
		- Penyederhanaan proses transaksi ke database, hanya menggunakan single insert query
		- CALL API : /mmrtsi/api/get_area.php?kar_idpembaca=kihiang
	# Release
*/

	header("Content-Type: text/json; charset=UTF8");

	require('../logging.php');
	require('../setDB01.php');
	$log    = new errorLog();

	$stat_entry = true;
	$stat_get 	= false;
	$stat_mess 	= "";
	$kp_kode	= "NN";
	if(!isset($_GET['kar_idpembaca'])){
		$stat_entry = false ; 
		$stat_mess 	= "Tidak ada data untuk diunduh "; 
	}
	else{
		$kar_idpembaca 	= $_GET['kar_idpembaca'];
	}

	define('_USER',$kar_idpembaca);
	define('_KODE','login');
	define('_TOKN', uniqid());
	define('_HOST',$_SERVER['REMOTE_ADDR']);

	if($stat_entry){
		try{
			$data = array();
			$que = "SELECT a.dkd_kd AS warea_kd,a.dkd_kd,SUBSTR(a.dkd_kd,1,2) AS kp_kode,a.dkd_tcatat AS warea_tgl_baca,a.dkd_jalan AS warea_nama,a.kar_id AS kar_idpembaca FROM pdam_gart.tr_dkd a JOIN pdam_gart.tm_karyawan b ON(b.kp_kode=SUBSTR(a.dkd_kd,1,2) AND b.kar_id=a.kar_id) WHERE a.dkd_tcatat>0 AND a.kar_id='".$kar_idpembaca."'";
			foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
				$data[]	= $row;
			}
			echo json_encode($data);
		}
		catch (Exception $e){
			$log->logMess('Terjadi kesalahan pada basis data');
			$log->errorDB($e->getMessage());
			$log->logDB($que);
			header("HTTP/1.1 500 Internal Server Error");
		    echo json_encode(array('reference_area'=>$e->getMessage()));
		}
	}
	else{
		echo json_encode('Error parameter: '.$stat_mess);		 
	}
	flush();
?>
