<?php

/* 

Model : API 
Fungsi : Send data Bacaan stand meter 

CALL API : push_dsml.php?f_wmmr_id=&f_wdsml_pel_no=000001&f_wdsml_bln_baca=04&f_wdsml_thn_baca=2013&f_wmmr_tgl_baca=2013/04/01&f_wmmr_abnormwm=0&f_wmmr_abnormenv=0&f_kar_id=admin&f_wmmr_standbaca=3232&f_wmmr_no_hp_baca=08234&f_lonkor=-6.935117603625825&f_latkor=107.63588905334473            
 

CALL store procedur : 
CALL p_entry_wm('$f_wdsml_pel_no','$f_wmmr_id ','$f_wmmr_no_hp_baca','$f_wmmr_tgl_baca','1','$f_wmmr_abnormwm','$f_wmmr_abnormenv','$f_kar_id','$f_wmmr_standbaca','$f_wmmr_note',$f_lonkor,$f_latkor,@hasilnya)";
SELECT  @devname,@devnote ;

FIELD YANG DI ENTRY             
------------------ 
wmmr_id            
wdsml_pel_no       
wdsml_bln_baca     
wdsml_thn_baca     
wmmr_tgl_baca      
wmmr_abnormwm      
wmmr_abnormenv     
kar_id             
wmmr_standbaca     
wmmr_no_hp_baca    
wmmr_sts_baca      
wmmr_tgl_validasi  
wmmr_sts_validator 
wmmr_sts           
lonkor             
latkor      
 
                
*/ 

require('../config.inc.php');  
require('../function.php');

/*
  
http://localhost/pdam_wmmr/api/push_dsml.php?f_wmmr_id=&f_wdsml_pel_no=IN195643&f_wdsml_bln_baca=11&f_wdsml_thn_baca=2013&f_wmmr_tgl_baca=2013/11/21&f_wmmr_abnormwm=0&f_wmmr_abnormenv=0&f_kar_id=admin&f_wmmr_standbaca=3232&f_wmmr_no_hp_baca=08234&f_latkor=0&f_lonkor=0
                  
  */            
                
$stat_entry = false ; 
                
				$f_wmmr_id             =  $_GET['f_wmmr_id'];            
				$f_wdsml_pel_no        =  $_GET['f_wdsml_pel_no'];       
				$f_wdsml_bln_baca      =  $_GET['f_wdsml_bln_baca'];     
				$f_wdsml_thn_baca      =  $_GET['f_wdsml_thn_baca'];     
				$f_wmmr_tgl_baca       =  $_GET['f_wmmr_tgl_baca'];      
				$f_wmmr_abnormwm       =  $_GET['f_wmmr_abnormwm'];      
				$f_wmmr_abnormenv      =  $_GET['f_wmmr_abnormenv'];     
				$f_kar_id              =  $_GET['f_kar_id'];             
				$f_wmmr_standbaca      =  $_GET['f_wmmr_standbaca'];     
				$f_wmmr_no_hp_baca     =  $_GET['f_wmmr_no_hp_baca'];    
				$f_wmmr_sts_baca       =  $_GET['f_wmmr_sts_baca'];      
				$f_wmmr_tgl_validasi   =  $_GET['f_wmmr_tgl_validasi'];  
				$f_wmmr_sts_validator  =  $_GET['f_wmmr_sts_validator']; 
				$f_wmmr_sts            =  $_GET['f_wmmr_sts'];           
				$f_wmmr_note           =  $_GET['f_wmmr_note'];           
				$f_lonkor              =  $_GET['f_lonkor'];             
				$f_latkor              =  $_GET['f_latkor'];             
 
/* cek, apakah perangkat terdaftar ? */


/* Jika terdaftar apakah user&passwordnya valid */


/* jika okeh semua, baru query deh */

$stat_entry = true  ; 

 
	 

       $msg = "";
       if((empty($f_kar_id)) && (empty($f_wdsml_pel_no))) {
                $msg = "\nPeringatan: \nNomor SL: $wmmr_pelno \ndata yang terkirim salah/kurang lengkap,\npengiriman dibatalkan!";
       } else {
 
               $read_conn1 = Write_connect() ;
               $SQL_ENTRY = "CALL p_entry_wm('$f_wdsml_pel_no','$f_wmmr_id ','$f_wmmr_no_hp_baca','$f_wmmr_tgl_baca','1','$f_wmmr_abnormwm','$f_wmmr_abnormenv','$f_kar_id','$f_wmmr_standbaca','$f_wmmr_note',$f_lonkor,$f_latkor,@hasilnya)";
    
               $rs = mysql_query($SQL_ENTRY) ;
  
								if (mysql_errno()) { 
												/* Log,On  error  */
												               $read_conn1 = Write_connect() ;
												               $data_log = "$f_wmmr_id|$f_wdsml_pel_no|$f_wdsml_bln_baca|$f_wdsml_thn_baca|$f_wmmr_tgl_baca|$f_wmmr_abnormwm|$f_wmmr_abnormenv|$f_kar_id|$f_wmmr_standbaca|$f_wmmr_no_hp_baca|$f_wmmr_sts_baca|$f_wmmr_tgl_validasi|$f_wmmr_sts_validator|$f_wmmr_sts|$f_wmmr_note |$f_lonkor|$f_latkor" ;
												               $log_sql = "INSERT INTO tr_log(log_date,log_data) VALUES (now(),'LOG:".$data_log."')  " ;												
												               $send_log  = mysql_query($log_sql) ;
												
												/* End log error */               

	 										  /* jika ada yang error, kasih tauu */
												   $msg[] = array(
																			'Pesan'  => "Data Tidak Tersimpan" ,
																			'Nomor_SL'=>$f_wdsml_pel_no 
																		);  
								
										    header("HTTP/1    .1 500 Internal Server Error");
 
											  echo json_encode(array('reference_dsml' =>mysql_error() ));
											  exit ;
										}else {
												/* Log, apapun yg di send akan di simpen */
												               $read_conn1 = Write_connect() ;
												               $data_log = "$f_wmmr_id|$f_wdsml_pel_no|$f_wdsml_bln_baca|$f_wdsml_thn_baca|$f_wmmr_tgl_baca|$f_wmmr_abnormwm|$f_wmmr_abnormenv|$f_kar_id|$f_wmmr_standbaca|$f_wmmr_no_hp_baca|$f_wmmr_sts_baca|$f_wmmr_tgl_validasi|$f_wmmr_sts_validator|$f_wmmr_sts|$f_wmmr_note |$f_lonkor|$f_latkor" ;
												               $log_sql = "INSERT INTO tr_log(log_date,log_data) VALUES (now(),'ERR:".$data_log."')  " ;												
												               $send_log  = mysql_query($log_sql) ;
												
												/**/               

														$msg[] = array(
																			'Pesan'  => "Data Tersimpan" ,
																			'Karyawan_ID'=>$f_kar_id,
																			'Nomor_SL'=>$f_wdsml_pel_no 
																		);   
												           // header("Content-Type: text/json"); // Dirubah Jadi json
													echo json_encode(array('send_respon' => $msg));
											  										
											} 
								       
								 
       }       
  
  
  ?>
