<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	//header('Access-Control-Allow-Origin: *');
	
	// inquiri resume bacaan
	require "setDB.php";
	try{
		$que 			= "SELECT
								e.*, d.out_nama,
								k.konsumsi
							FROM
								(
									SELECT
										b.out_id,
										IFNULL(
											DAY (MAX(c.wmmr_tgl_baca)),
											32
										) AS wdsml_tgl_baca,
										IFNULL(
											DATE_FORMAT(
												MAX(c.wmmr_tgl_baca),
												'%d-%m-%Y %H:%i:%s'
											),
											'00-00-0000 00:00:00'
										) AS wdsml_last_baca,
										COUNT(*) AS wdsml_total,
										SUM(

											IF (ISNULL(c.wdsml_pel_no), 0, 1)
										) AS wdsml_diisi,

									IF (
										(
											c.wmmr_standbaca >= a.wdsml_sm_akhir
										),
										(
											c.wmmr_standbaca - a.wdsml_sm_akhir
										),
										0
									) AS konsumsi
									FROM
										tm_wmmr_dsml a
									JOIN tr_wmmr_area b ON (
										b.dkd_kd = a.wdsml_dkd_kd
										AND b.warea_bln_baca = a.wdsml_bln_baca
										AND b.warea_thn_baca = a.wdsml_thn_baca
									)
									LEFT JOIN tm_wmmr_sm c ON (
										c.wdsml_pel_no = a.wdsml_pel_no
										AND c.wdsml_bln_baca = a.wdsml_bln_baca
										AND c.wdsml_thn_baca = a.wdsml_thn_baca
									)
									WHERE
										a.wdsml_bln_baca = DATE_FORMAT(NOW(), '%m')
									AND a.wdsml_thn_baca = YEAR (NOW())
									GROUP BY
										b.out_id,
										DAY (c.wmmr_tgl_baca)
								) e
							JOIN tr_outsource d ON (d.out_id = e.out_id)
							JOIN v_konsumsi_dkd k ON (k.wdsml_kp_kode = e.out_id)";
		
		$sth 	= $link->prepare($que);
		$sth->execute();
		while($row = $sth->fetch(PDO::FETCH_ASSOC)){
			$data[$row['out_id']]['out_nama'] 								= $row['out_nama'];
			$data[$row['out_id']]['wdsml_last']								= $row['wdsml_last_baca'];
			$data[$row['out_id']]['wdsml_total'][$row['wdsml_tgl_baca']]	= $row['wdsml_total'];
			$data[$row['out_id']]['wdsml_diisi'][$row['wdsml_tgl_baca']]	= $row['wdsml_diisi'];
			$data[$row['out_id']]['konsumsi']								= $row['konsumsi'];
			$wdsml_diisi[$row['out_id']][] 									= 0;
		}
		$link	= null;
	}
	catch(Exception $e){
		//$log->errorDB($e->getMessage());
		//$log->logMess("Gagal melakukan inquiri data rekening");
		//$log->logDB($que);
	}	
	
	$data_key 		= array_keys($data);
	for($i=0;$i<count($data_key);$i++){
		$data_val[$data_key[$i]]['out_nama'] 		= $data[$data_key[$i]]['out_nama'];
		$data_val[$data_key[$i]]['wdsml_last']		= $data[$data_key[$i]]['wdsml_last'];
		$data_val[$data_key[$i]]['wdsml_total']		= array_sum($data[$data_key[$i]]['wdsml_total']);
		$data_val[$data_key[$i]]['wdsml_sisa']		= abs($data[$data_key[$i]]['wdsml_total'][32]);
		$data_val[$data_key[$i]]['konsumsi']		= $data[$data_key[$i]]['konsumsi'];
		for($j=1;$j<=date('t');$j++){
			$wdsml_diisi[$data_key[$i]][]				= $data[$data_key[$i]]['wdsml_diisi'][$j];
			$data_val[$data_key[$i]]['wdsml_diisi'][$j]	= array_sum($wdsml_diisi[$data_key[$i]]);
		}
	}
	
	$rand_key 	= array_keys($data_val);
	for($i=0;$i<count($rand_key);$i++){
		$data_str[] = json_encode($data_val[$rand_key[$i]]);
	}
	
	echo "data: [".implode(",",$data_str)."]\n\n";	
	
	flush();
?>

