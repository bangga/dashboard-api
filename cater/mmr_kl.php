<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	// inquiri resume bacaan
	require "setDB.php";
	try{
		$que 	= "SELECT COUNT(*) AS reff,CONCAT(a.wdsml_bln_baca,'-',a.wdsml_thn_baca) AS wdsml_bln_baca,b.kl_kd,b.kl_ket FROM tm_wmmr_sm a JOIN tr_kondisi_lingkungan b ON(b.kl_kd=a.wmmr_abnormenv) WHERE CONCAT(a.wdsml_thn_baca,a.wdsml_thn_baca)>DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 12 MONTH),'%Y%m') GROUP BY a.wmmr_abnormenv,a.wdsml_thn_baca,a.wdsml_bln_baca";
		$sth 	= $link->prepare($que);
		$sth->execute();
		while($row = $sth->fetch(PDO::FETCH_ASSOC)){
			// $data['kl_kode2'][0]						= 'Normal';
			// $data['kl_kode2'][1]						= 'Fault Code';
			$data['kl_kode2'][$row['kl_kd']] 						= $row['kl_kd'];
			$data['reff2'][$row['wdsml_bln_baca']][$row['kl_kd']]	= $row['reff'];
			$data['total2'][$row['wdsml_bln_baca']][]				= $row['reff'];
			if($row['kl_kd']!=0){
				// data komposisi fault code
				$data['kl_kode'][$row['kl_ket']] 						= $row['kl_ket'];
				$data['reff'][$row['wdsml_bln_baca']][$row['kl_ket']]	= $row['reff'];
				$data['total'][$row['wdsml_bln_baca']][]				= $row['reff'];

				// data komposisi total
				// $data['reff2'][$row['wdsml_bln_baca']][1]			= $row['reff'];
			}
/* 			else{
				// data komposisi total
				$data['reff2'][$row['wdsml_bln_baca']][0]				= $row['reff'];
			} */
		}
		$link	= null;
	}
	catch(Exception $e){
		//$log->errorDB($e->getMessage());
		//$log->logMess("Gagal melakukan inquiri data rekening");
		//$log->logDB($que);
	}

	$date 		= new DateTime('NOW');
	$bulan[]	= $date->format('m-Y');
	for($i=0;$i<11;$i++){
		$date->sub(new DateInterval('P1M'));
		$bulan[] 	= $date->format('m-Y');
	}

	$k			= 1;
	for($i=11;$i>=0;$i--){
		// data komposisi fault code
		$data_val	= $data['kl_kode'];
		$data_key	= array_keys($data_val);
		for($j=0;$j<count($data_key);$j++){
			$data_new[$j] 		= $data_val[$data_key[$j]];
			if(isset($data['total'][$bulan[$i]])){
				$data_sum			= array_sum($data['total'][$bulan[$i]]);
				$data_sum2			= array_sum($data['total2'][$bulan[$i]]);
				$data2_new[$j][]	= round(100*$data['reff'][$bulan[$i]][$data_key[$j]]/$data_sum2,4);
			}
			else{
				$data2_new[$j][]	= 0;
			}
		}

		// data komposisi total
		$data_val	= $data['kl_kode2'];
		$data_key	= array_keys($data_val);
		for($j=0;$j<count($data_key);$j++){
			$data4_new[$j] 		= $data_val[$data_key[$j]];
			if(isset($data['total2'][$bulan[$i]])){
				$data_sum			= array_sum($data['total2'][$bulan[$i]]);
				// $data5_new[$j][]	= round(100*$data['reff2'][$bulan[$i]][$data_key[$j]]/$data_sum,4);
				$data5_new[$j][]	= abs($data['reff2'][$bulan[$i]][$data_key[$j]]);
			}
			else{
				$data5_new[$j][]	= 0;
			}
		}
		$data3_new[]	= array($k,$bulan[$i]);
		$k++;
	}

	unset($data);
	// data komposisi fault code
	$data['kl_kode']	= $data_new;
	$data['reff']		= $data2_new;

	// data komposisi total
	// $data['kl_kode2']	= $data4_new;
	$data['reff2']		= $data5_new;

	$data['bulan']		= $data3_new;
	echo json_encode($data);
	flush();
?>

