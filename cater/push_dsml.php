<?php
/*
	Change Log
	# Migrasi server kerinci ke vps [18-03-2016]
		- Penyederhanaan query transaksi, wmmr_id menggunakan uuid
	# Propose [30-04-2014]
		- Logging diarahkan ke sqlite
		- Penyederhanaan proses transaksi ke database, hanya menggunakan single insert query
	# Release
	Model : API
	Fungsi : Send data Bacaan stand meter

	CALL API : push_dsml.php?f_wmmr_id=&f_wdsml_pel_no=000001&f_wdsml_bln_baca=04&f_wdsml_thn_baca=2013&f_wmmr_tgl_baca=2013/04/01&f_wmmr_abnormwm=0&f_wmmr_abnormenv=0&f_kar_id=admin&f_wmmr_standbaca=3232&f_wmmr_no_hp_baca=08234&f_lonkor=-6.935117603625825&f_latkor=107.63588905334473

	CALL store procedur :
	CALL p_entry_wm('$f_wdsml_pel_no','$f_wmmr_id ','$f_wmmr_no_hp_baca','$f_wmmr_tgl_baca','1','$f_wmmr_abnormwm','$f_wmmr_abnormenv','$f_kar_id','$f_wmmr_standbaca','$f_wmmr_note',$f_lonkor,$f_latkor,@hasilnya)
	SELECT  @devname,@devnote ;

	FIELD YANG DI ENTRY
	------------------
	wmmr_id
	wdsml_pel_no
	wdsml_bln_baca
	wdsml_thn_baca
	wmmr_tgl_baca
	wmmr_abnormwm
	wmmr_abnormenv
	kar_id
	wmmr_standbaca
	wmmr_no_hp_baca
	wmmr_sts_baca
	wmmr_tgl_validasi
	wmmr_sts_validator
	wmmr_sts
	lonkor
	latkor
*/

/** getParam
    memindahkan semua nilai dalam array GET ke dalam variabel yang bersesuaian dengan masih kunci array
*/
	$nilai = $_GET;
	$konci = array_keys($nilai);
	for($i=0;$i<count($konci);$i++){
		$$konci[$i] = $nilai[$konci[$i]];
	}
/*  getParam
**/

	define('_USER',$f_kar_id);
	define('_KODE','00PROD');
	define('_TOKN', uniqid());
	define('_HOST',$_SERVER['REMOTE_ADDR']);

	require('../logging.php');
	require('../setDB01.php');
	$log    = new errorLog();

    // http://localhost/mmr_test/api/push_dsml.php?f_wmmr_id=&f_wdsml_pel_no=IN164606&f_wdsml_bln_baca=04&f_wdsml_thn_baca=2014& f_wmmr_tgl_baca=2014/04/24+06:49:45&f_wmmr_abnormenv=1%20&f_wmmr_note=&f_kar_id=dei08&f_wmmr_standbaca=492&f_wmmr_no_hp_baca=085659651335&f_lonkor=104.05849666666667&f_latkor=1.0342283333333335

    if((empty($f_kar_id)) || empty($f_wdsml_pel_no)) {
		$pesan = "\nPeringatan: \nNomor SL: $wmmr_pelno \ndata yang terkirim salah/kurang lengkap,\npengiriman dibatalkan!";
		// Pesan turunan
		$msg[]  = array(
				'Pesan'  => $pesan ,
				'Karyawan_ID'=>$f_kar_id,
				'Nomor_SL'=>$f_wdsml_pel_no
			);
		echo json_encode(array('send_respon' => $msg));
	}
	else{
		try {
			$PLINK->beginTransaction();
			$que    = "INSERT INTO caterpdam.tm_wmmr_sm(wmmr_id,wdsml_pel_no,wdsml_bln_baca,wdsml_thn_baca,wmmr_tgl_baca,wmmr_abnormwm,wmmr_abnormenv,kar_id,wmmr_standbaca,wmmr_no_hp_baca,wmmr_sts_baca,wmmr_sts_validator,wmmr_sts,wmmr_note,lonkor,latkor,remark_id) VALUES('"._TOKN."','".$f_wdsml_pel_no."','".$f_wdsml_bln_baca."','".$f_wdsml_thn_baca."',STR_TO_DATE('".$f_wmmr_tgl_baca."','%Y/%m/%d %H:%i:%s'),'".$f_wmmr_abnormwm."','".$f_wmmr_abnormenv."','"._USER."',".$f_wmmr_standbaca.",'".$f_wmmr_no_hp_baca."',1,0,1,'".$f_wmmr_note."',".$f_lonkor.",".$f_latkor.",'"._TOKN."') ON DUPLICATE KEY UPDATE wmmr_tgl_baca=STR_TO_DATE('".$f_wmmr_tgl_baca."','%Y/%m/%d %H:%i:%s'),wmmr_abnormwm='".$f_wmmr_abnormwm."',wmmr_abnormenv='".$f_wmmr_abnormenv."',kar_id='"._USER."',wmmr_standbaca=".$f_wmmr_standbaca.",wmmr_no_hp_baca='".$f_wmmr_no_hp_baca."',wmmr_note='".$f_wmmr_note."',lonkor=".$f_lonkor.",latkor=".$f_latkor.",remark_id='"._TOKN."'";
			$res    = $PLINK->exec($que);
			$PLINK->commit();

			if($res>0){
				$log->logMess("Input DSML telah berhasil dilakukan");
				$log->logDB($que);
				$pesan = "Data Tersimpan";
			}
			else{
				$log->logMess("Input DSML tidak dapat dilakukan");
				$pesan = "Data Tersimpan";
			}
			
			// Pesan turunan
			$msg[]  = array(
					'Pesan'  => $pesan ,
					'Karyawan_ID'=>$f_kar_id,
					'Nomor_SL'=>$f_wdsml_pel_no
				);
			echo json_encode(array('send_respon' => $msg));
		}
		catch (Exception $e){
			$PLINK->rollBack();
			$log->logMess("Input DSML gagal dilakukan");
			$log->errorDB($e->getMessage());
			$log->logDB($que);

			// Pesan turunan
			$msg[] = array(
					'Pesan'  => "Data Tidak Tersimpan" ,
					'Nomor_SL'=>$f_wdsml_pel_no
				);
			header("HTTP/1.1 500 Internal Server Error");
			echo json_encode(array('reference_dsml' =>$msg ));
		}
	}
	$PLINK   = null;
?>
