<?php
/*
	Change Log
	# Migrasi server kerinci ke vps [31-03-2016]
		- Penyederhanaan query transaksi
		- Logging diarahkan ke sqlite
		- Penyederhanaan proses transaksi ke database, hanya menggunakan single insert query
		- CALL API : /mmrtsi/api/get_dsml_checker.php?prm=1&area_kd=C01R013601&area_kd=C01R013601&bln=03&thn=2016&device_id=6f0d289d976661c0406ca9423b6134ac&user_id=hing000&pass=06224
	# Release
*/

	header("Content-Type: text/json; charset=UTF8");

/** getParam
    memindahkan semua nilai dalam array GET ke dalam variabel yang bersesuaian dengan masih kunci array
*/
	$nilai = $_GET;
	$konci = array_keys($nilai);
	for($i=0;$i<count($konci);$i++){
		$$konci[$i] = $nilai[$konci[$i]];
	}
/*  getParam
**/

	// filter data
	if($prm=="1"){
		$queryparam = "AND ISNULL(b.wmmr_sts)";
	}
	else if($prm == "2"){
		$queryparam = "AND b.wmmr_sts=1 and b.wmmr_sts_baca=0 and b.wmmr_sts_validator=0";
	}

	define('_USER',$user_id);
	define('_KODE','login');
	define('_TOKN', uniqid());
	define('_HOST',$_SERVER['REMOTE_ADDR']);

	require('../../logging.php');
	require('../../setDB01.php');
	$log    = new errorLog();

	/* Cek parameter */
	$stat_get = false;
	$err_note = "Device not Register ";
	
	try{
		// cek device id di basis data
		$que = "SELECT per_id FROM mmr_tsi.tm_perangkat WHERE per_id ='".$device_id."'";
		foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
			$err_note = "Device is Registered ";
		}

		// cek user id di basis data
		$que = "SELECT kar_id FROM mmr_tsi.tm_karyawan WHERE kar_id ='".$user_id."' AND kar_pass=MD5('".$pass."')";
		foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
			$stat_get = true;
		}
		if(!$stat_get){
			$err_note .="| Username or Password not match ";
		}
		
		// cek jadwal baca
		// skip
		// $err_note .="| This Area cannot read today : ".date('d');
		
		// download order
		if($stat_get){
			$data	= array();
			$que 	= "SELECT a.wdsml_sequence,c.warea_kd AS wrute_id,a.wdsml_pel_no,c.warea_kd,c.warea_kd AS wrute_entry,a.wdsml_pel_nama,a.wdsml_pel_alamat,a.wdsml_pel_nowm,a.wdsml_dkd_kd,a.wdsml_dkd_no,a.wdsml_kp_kode,a.wdsml_bln_baca,a.wdsml_thn_baca,a.lonkor,a.latkor,'' AS wdsml_stand_akhir,a.wdsml_pakai_tinggi,a.wdsml_pakai_rendah,a.wdsml_pel_no AS pel_no,a.wdsml_bln_baca AS bln_baca,a.wdsml_thn_baca AS thn_baca FROM mmr_tsi.tm_wmmr_dsml a LEFT JOIN mmr_tsi.tm_wmmr_sm b ON(a.wdsml_pel_no=b.wdsml_pel_no AND a.wdsml_thn_baca=b.wdsml_thn_baca AND a.wdsml_bln_baca=b.wdsml_bln_baca) LEFT JOIN mmr_tsi.tr_wmmr_area c ON (c.dkd_kd = a.wdsml_dkd_kd AND c.warea_bln_baca = a.wdsml_bln_baca AND c.warea_thn_baca = a.wdsml_thn_baca) WHERE c.warea_kd='".$area_kd."' AND a.wdsml_bln_baca='".$bln."' AND a.wdsml_thn_baca='".$thn."' ".$queryparam." ORDER BY wdsml_sequence ASC";
			foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
				$data[]	= $row;
			}
			echo json_encode($data);
		}
		else{
			echo json_encode('Error parameter:'.$err_note);
		}
	}
	catch (Exception $e){
		$log->logMess('Terjadi kesalahan pada basis data');
		$log->errorDB($e->getMessage());
		$log->logDB($que);
		header("HTTP/1.1 500 Internal Server Error");
		echo json_encode(array('reference_area'=>$e->getMessage()));
	}

	flush();
?>
