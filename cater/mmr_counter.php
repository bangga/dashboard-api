<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	//header('Access-Control-Allow-Origin: *');
	
	// inquiri data bacaan terakhir
	require "setDB.php";
	try{
		$que 			= "SELECT DATE_FORMAT(MAX(IFNULL(b.wmmr_tgl_baca,'0000-00-00 00:00:00')),'%d-%m-%Y %H:%i:%s') AS dsmlRemark,SUM(a.wdsml_pel_sts) AS dsmlBulanan,SUM(IF(ISNULL(b.wmmr_id),0,1)) AS dsmlDibaca,SUM(IF(b.wmmr_sts_validator=3,1,0)) AS dsmlDisettle,SUM(IF(ISNULL(b.wmmr_id),1,0)) AS dsmlKosong,SUM(IF(IFNULL(b.wmmr_standbaca,0)<a.wdsml_pakai_rendah,1,0)) AS dsmlRendah,SUM(IF((IFNULL(b.wmmr_standbaca,0)<a.wdsml_pakai_rendah AND b.wmmr_sts_validator=3),1,0)) AS settleRendah,SUM(IF(IFNULL(b.wmmr_standbaca,0)>a.wdsml_pakai_tinggi,1,0)) AS dsmlTinggi,SUM(IF((IFNULL(b.wmmr_standbaca,0)>a.wdsml_pakai_tinggi AND b.wmmr_sts_validator=3),1,0)) AS settleTinggi,SUM(IF(IFNULL(b.wmmr_standbaca,0)<a.wdsml_stand_akhir,1,0)) AS dsmlNegatif,SUM(IF((IFNULL(b.wmmr_standbaca,0)<a.wdsml_stand_akhir AND b.wmmr_sts_validator=3),1,0)) AS settleNegatif,SUM(IF(DATE(b.wmmr_tgl_baca)=CURDATE(),1,0)) AS dsmlHarian FROM tm_wmmr_dsml a LEFT JOIN tm_wmmr_sm b ON(b.wmmr_id=a.wmmr_id) WHERE ABS(a.wdsml_bln_baca)=MONTH(CURDATE()) AND a.wdsml_thn_baca=YEAR(CURDATE())";
		$sth 			= $link->prepare($que);
		$sth->execute();
		$data_val		= $sth->fetch(PDO::FETCH_ASSOC);
		$link			= null;
	}
	catch(Exception $e){
		//$log->errorDB($e->getMessage());
		//$log->logMess("Gagal melakukan inquiri data rekening");
		//$log->logDB($que);
	}
	
	echo "data: ".json_encode($data_val)."\n\n";
	
	flush();
?>
