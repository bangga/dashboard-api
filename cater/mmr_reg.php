<?php

/*
	Change Log
	# Migrasi server kerinci ke vps [30-03-2016]
		- Penyederhanaan query transaksi
		- Logging diarahkan ke sqlite
		- Penyederhanaan proses transaksi ke database, hanya menggunakan single insert query
		- CALL API : /mmrtsi/api/mmr_reg.php?user_id=admin&user_pass=12345678&dev_imei=351816055023869&dev_mac=null
	# Release
		Model : API 
		Fungsi : Cek apakah devices sudah ter-register 

		Parameter : 
		 - user_id     : user ID
		 - user_pass   : PAssword
		 - dev_imei    : no. Imei 
		 - dev_mac     : mac Address

		Return : 
						'sts_reg'  =>$sts_reg,
						'r_devid'  =>$r_devid,  
						'r_devname'=>$r_devname,
						'r_devnote'=>$r_devnote 
													
													 
		Call API : mmr_reg.php?user_id=admin&user_pass=123&dev_imei=0987654321&dev_mac=09:992:992:992 

		CAll Store Procedur : 
		CALL p_api_hw_reg("admin","123","0987654321","","09:992:992:992",@devid,@devname,@devnote) ;
		SELECT @devid,@devname,@devnote ;
*/

/** getParam
    memindahkan semua nilai dalam array GET ke dalam variabel yang bersesuaian dengan masih kunci array
*/
	$nilai = $_GET;
	$konci = array_keys($nilai);
	for($i=0;$i<count($konci);$i++){
		$$konci[$i] = $nilai[$konci[$i]];
	}
/*  getParam
**/

	define('_USER',$user_id);
	define('_KODE','login');
	define('_TOKN', uniqid());
	define('_HOST',$_SERVER['REMOTE_ADDR']);

	require('../logging.php');
	require('../setDB01.php');
	$log    = new errorLog();

	/* Cek parameter */
	$stat_entry	= true;
    $sts_reg 	= false;
	$stat_mess 	= "";
  
	if (!isset($user_id) and $user_id == "") { 
		$stat_entry = false ; 
		$stat_mess 	= "User ID null ".$user_id ; 
	}
	if (!isset($user_pass)) { 
		$stat_entry = false ; 
		$stat_mess .= ",User pass null ";
	}
	if (!isset($dev_imei)) { 
		$stat_entry = false ; 
		$stat_mess .= ",IMEI null " ; 
	}
	if (!isset($dev_mac) AND $dev_mac == "") { 
		$stat_entry = false ; 
		$stat_mess .= ",Mac Address null " ; 
	}
  
    if($stat_entry){
       	try{
			$r_devid	= "null";
			$r_devname 	= "null";
			$r_devnote 	= "user Invalid";
			$kar_sts	= 0;
			$que		= "SELECT IF(ISNULL(kar_id),0,1) AS kar_sts FROM pdam_gart.tm_karyawan WHERE kar_id='".$user_id."' AND kar_pass=md5('".$user_pass."')";
			foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
				$r_devnote 	= "hw not register";
				$kar_sts	= $row['kar_sts'];
			}

			if($kar_sts==1){
				$que	= "SELECT IF(ISNULL(per_id),0,1) AS per_sts,per_id,per_nama FROM caterpdam.tm_perangkat WHERE per_imei='".$dev_imei."' AND per_sts=1";
				foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
					$r_devname 	= $row['per_nama'];
					$r_devid	= $row['per_id'];
					$r_devnote 	= "hw registered";
				}
			}

			if (strlen($r_devid)>5){
				$sts_reg =  "true";
			}

			$json[] = array(
				'sts_reg'=>$sts_reg,
				'r_devid'=>$r_devid,  
				'r_devname'=>$r_devname,
				'r_devnote'=>$r_devnote 
			);

			$log->logMess($r_devnote.' '.$r_devname.' '.$r_devid);
		}
		catch (Exception $e){
			$log->logMess('Terjadi kesalahan pada basis data');
			$log->errorDB($e->getMessage());
			$log->logDB($que);
			$stat_mess = "Error Update Data! \n IMEI: ".$dev_id;
			$json[] = array(
				'sts_reg'=>$sts_reg,
				'r_devnote'=>$stat_mess 
			); 
		}
	}
	else{
		$json[] = array(
			'sts_reg'=>$sts_reg,
			'r_devnote'=>$stat_mess 
		); 
	}
      
    /* Send JSON */
	header("Content-Type: text/json");
	echo json_encode(array('device_register' => $json));
?>
