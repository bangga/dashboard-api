<?php

/* 
	Change Log
	# Migrasi server kerinci ke vps [30-03-2016]
		- Penyederhanaan query transaksi
		- Logging diarahkan ke sqlite
		- Penyederhanaan proses transaksi ke database, hanya menggunakan single insert query
		- CALL API : /mmrtsi/api/mmr_login.php?user_id=wawan&user_pass=wawan&dev_id=b288fadb5311fb7bbbbeb19f6dc152bf
	# Release
		Model : API 
		Fungsi : Untuk login ke aplikasi 

		CALL API : mmr_login.php?user_id=admin&user_pass=123&dev_id=0987654321 

		CALL store procedur : 
		CALL p_api_hw_login("admin","123","ad68f06fa9beb3bf0eb710227a610cd3",@devname,@devnote) ;
		SELECT  @devname,@devnote ;
*/            

/** getParam
    memindahkan semua nilai dalam array GET ke dalam variabel yang bersesuaian dengan masih kunci array
*/
	$nilai = $_GET;
	$konci = array_keys($nilai);
	for($i=0;$i<count($konci);$i++){
		$$konci[$i] = $nilai[$konci[$i]];
	}
	
/*  getParam
**/

	define('_USER',$user_id);
	define('_KODE','login');
	define('_TOKN', uniqid());
	define('_HOST',$_SERVER['REMOTE_ADDR']);

	require('../logging.php');
	require('../setDB01.php');
	$log    = new errorLog();

	/* Cek parameter */
	$stat_entry	= true;
    $sts_reg 	= false;
	$stat_mess 	= "";
 
	if (!isset($user_id) and $user_id == "") { 
		$stat_entry = false ; 
		$stat_mess 	= "User ID null ".$user_id;
	}
	if (!isset($user_pass)) { 
		$stat_entry = false ; 
		$stat_mess .= ",User pass null " ; 
	}	 
 
	if (!isset($dev_id) AND $dev_id == "") { 
		$stat_entry = false ; 
		$stat_mess .= ",Device ID null " ; 
	}

	if($stat_entry){
       	try{
			$r_devname 	= "null";
			$r_devnote 	= "user Invalid";
			$kar_sts	= 0;
			$que		= "SELECT IF(ISNULL(kar_id),0,1) AS kar_sts FROM pdam_gart.tm_karyawan WHERE kar_id='".$user_id."' AND kar_pass=md5('".$user_pass."')";
			foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
				$r_devnote 	= "hw not register";
				$kar_sts	= $row['kar_sts'];
			}

			if($kar_sts==1){
				$que	= "SELECT IF(ISNULL(per_id),0,1) AS per_sts,per_nama FROM caterpdam.tm_perangkat WHERE per_id='".$dev_id."' AND per_sts=1";
				foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
					$r_devname = $row['per_nama'];
					$r_devnote = "hw registered";
				}
			}

			if (strlen($r_devname) > 2 ){
				$sts_reg =  "true";
			}

			$json[] = array(
				'sts_reg'  =>$sts_reg,
				'r_devname'=>$r_devname,
				'r_devnote'=>$r_devnote 
			);

			$log->logMess($r_devnote.' '.$r_devname);
		}
		catch (Exception $e){
			$log->logMess('Terjadi kesalahan pada basis data');
			$log->errorDB($e->getMessage());
			$log->logDB($que);
			$stat_mess = "Error Update Data! \n IMEI: ".$dev_id;
			$json[] = array(
				'sts_reg'=>$sts_reg,
				'r_devnote'=>$stat_mess 
			); 
		}
    }
	else{
		$json[] = array(
			'sts_reg'=>$sts_reg,
			'r_devnote'=>$stat_mess 
		);
		$log->logMess($r_devnote.' '.$r_devname);
    }
       
    /* Send json */
 	header("Content-Type: text/json");
	echo json_encode(array('device_login' => $json));
?>
