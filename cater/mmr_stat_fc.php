<?php
$con = mysql_connect("localhost","mmr","mmr@tb");

if (!$con) {
  die('Could not connect: ' . mysql_error());
}

mysql_select_db("mmr_atb", $con);

$query = mysql_query("
			SELECT a.wdsml_kp_kode,c.kp_ket,
							COUNT(b.wdsml_pel_no) as dibaca,
							(COUNT(a.wdsml_pel_no) - COUNT(b.wdsml_pel_no)) AS belumdibaca,
							COUNT(CASE WHEN b.wmmr_abnormenv = 1  THEN b.wmmr_abnormenv = 1 END) AS normal,
							COUNT(CASE WHEN b.wmmr_abnormenv <> 1  THEN b.wmmr_abnormenv <> 1 END) AS abnormal,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'M1'  THEN b.wmmr_abnormenv = 'M1' END) AS M1,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'M2'  THEN b.wmmr_abnormenv = 'M2' END) AS M2,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'M3'  THEN b.wmmr_abnormenv = 'M3' END) AS M3,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'M4'  THEN b.wmmr_abnormenv = 'M4' END) AS M4,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'M5'  THEN b.wmmr_abnormenv = 'M5' END) AS M5,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'M6'  THEN b.wmmr_abnormenv = 'M6' END) AS M6,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'M7'  THEN b.wmmr_abnormenv = 'M7' END) AS M7,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'M8'  THEN b.wmmr_abnormenv = 'M8' END) AS M8,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'L1'  THEN b.wmmr_abnormenv = 'L1' END) AS L1,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'L2'  THEN b.wmmr_abnormenv = 'L2' END) AS L2,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'L3'  THEN b.wmmr_abnormenv = 'L3' END) AS L3,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'L4'  THEN b.wmmr_abnormenv = 'L4' END) AS L4,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'L5'  THEN b.wmmr_abnormenv = 'L5' END) AS L5,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'L6'  THEN b.wmmr_abnormenv = 'L6' END) AS L6,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'L7'  THEN b.wmmr_abnormenv = 'L7' END) AS L7,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'L8'  THEN b.wmmr_abnormenv = 'L8' END) AS L8,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'L9'  THEN b.wmmr_abnormenv = 'L9' END) AS L9,
							COUNT(CASE WHEN b.wmmr_abnormenv = 'RK'  THEN b.wmmr_abnormenv = 'RK' END) AS RK
			FROM tm_wmmr_dsml a
			LEFT JOIN tm_wmmr_sm b ON a.wdsml_pel_no = b.wdsml_pel_no AND a.wdsml_bln_baca = b.wdsml_bln_baca AND a.wdsml_thn_baca = b.wdsml_thn_baca
			LEFT JOIN tr_kota_pelayanan c ON a.wdsml_kp_kode = c.kp_kode
		   WHERE a.wdsml_kp_kode = '04'
		  AND a.wdsml_bln_baca = '03'
		  GROUP BY a.wdsml_kp_kode");

$category = array();
$category['name'] = 'Area Pelayanan';

$series1 = array();
$series1['name'] = 'Normal';

$series2 = array();
$series2['name'] = 'L1';

$series3 = array();
$series3['name'] = 'RK';


while($r = mysql_fetch_array($query)) {
    $category['data'][] = $r['kp_ket'];
    $series1['data'][] = $r['normal'];
    $series2['data'][] = $r['L1'];
    $series3['data'][] = $r['RK'];   
}

$result = array();
array_push($result,$category);
array_push($result,$series1);
array_push($result,$series2);
array_push($result,$series3);


print json_encode($result);

mysql_close($con);
?> 
