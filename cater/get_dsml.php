<?php
/*
	Change Log
	# Deployment tirtaintan [01-08-2016]
		- Perubahan download data rute, login offline
	# Deployment tirtaintan [14-07-2016]
		- Perubahan download data rute, berdasarkan jadwal pembaca meter
	# Migrasi server kerinci ke vps [31-03-2016]
		- Penyederhanaan query transaksi
		- Logging diarahkan ke sqlite
		- Penyederhanaan proses transaksi ke database, hanya menggunakan single insert query
		- CALL API : /mmrtsi/api/get_dsml.php?&area_kd=0A010101&area_kd=0A010101&bln=03&thn=2016&device_id=34b2b85db75491c1ae4dec663d66f9d2&user_id=kihiang&pass=kayuaro
	# Release
*/

	header("Content-Type: text/json; charset=UTF8");

/** getParam
    memindahkan semua nilai dalam array GET ke dalam variabel yang bersesuaian dengan masih kunci array
*/
	$nilai = $_GET;
	$konci = array_keys($nilai);
	for($i=0;$i<count($konci);$i++){
		$$konci[$i] = $nilai[$konci[$i]];
	}
/*  getParam
**/

	define('_USER',$user_id);
	define('_KODE','login');
	define('_TOKN', uniqid());
	define('_HOST',$_SERVER['REMOTE_ADDR']);

	require('../logging.php');
	require('../setDB01.php');
	$log    = new errorLog();

	/* Cek parameter */
	$stat_get = false;
	$kp_kode  = "00";
	$err_note = "Device not Register ";
	
	try{
		// cek device id di basis data
		$que = "SELECT per_id FROM caterpdam.tm_perangkat WHERE per_id ='".$device_id."'";
		foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
			$err_note = "Device is Registered ";
		}

		// cek user id di basis data
		$que = "SELECT kar_id,kp_kode FROM pdam_gart.tm_karyawan WHERE kar_id ='".$user_id."'";
		foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
			$stat_get = true;
			$kp_kode  = $row['kp_kode'];
		}
		if(!$stat_get){
			$err_note .="| Username or Password not match ";
		}
		
		// cek jadwal baca
		// skip
		// $err_note .="| This Area cannot read today : ".date('d');
		
		// download order
		if($stat_get){
			$data = array();
			$que = "SELECT * FROM caterpdam.v_wmmr_dsml WHERE kar_id='".$user_id."' AND SUBSTR(wdsml_dkd_kd,1,2)='".$kp_kode."' AND wdsml_bln_baca='".$bln."' AND wdsml_thn_baca='".$thn."' ORDER BY wdsml_sequence";
			foreach ($PLINK->query($que, PDO::FETCH_ASSOC) as $row){
				$data[]	= $row;
			}
			echo json_encode($data);
		}
		else{
			echo json_encode('Error parameter:'.$err_note);
		}
	}
	catch (Exception $e){
		$log->logMess('Terjadi kesalahan pada basis data');
		$log->errorDB($e->getMessage());
		$log->logDB($que);
		header("HTTP/1.1 500 Internal Server Error");
		echo json_encode(array('reference_area'=>$e->getMessage()));
	}

	flush();
?>
