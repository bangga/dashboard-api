<?php
	$mysqli = new mysqli("localhost","root","pohodeui","pdam_wmmrV3");
	
	if (mysqli_connect_errno())
	{
		printf("Can't connect to SQL Server. Error Code %s\n", mysqli_connect_error($mysqli));
		exit;
	}  

	// Set the default namespace to utf8
	$mysqli->query("SET NAMES 'utf8'");
	
	$json = array();
	
	if($result = $mysqli->query("SELECT * FROM tm_karyawan")) 
	{
		while ($row = $result->fetch_assoc())
		{
			$json[] = array(
				'kar_nik'=>$row['kar_nik'],
				'kar_nama'=>$row['kar_nama'],
			);
		}
	}
	
	$result->close();
 
	header("Content-Type: text/json");
	
	echo json_encode(array('reference_kar' => $json));
 
	$mysqli->close(); 
?>