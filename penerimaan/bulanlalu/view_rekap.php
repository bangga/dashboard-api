<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
    header('Access-Control-Allow-Origin: *');
	include $_SERVER['DOCUMENT_ROOT']."/api/setDB02.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	/* getParam **/

	/* database **/
	try {
        $que    = "SELECT IFNULL(SUM(b.rek_total),0) AS efes_lalu FROM tm_pembayaran a JOIN tm_rekening b ON(b.rek_nomor=a.rek_nomor) WHERE DATE(a.byr_upd_sts)<=DATE(DATE_SUB(NOW(), INTERVAL 1 MONTH)) AND b.rek_bln=MONTH(DATE_SUB(NOW(), INTERVAL 2 MONTH)) AND b.rek_thn=YEAR(DATE_SUB(NOW(), INTERVAL 2 MONTH)) AND a.byr_sts=1 AND b.rek_sts=1 AND rek_byr_sts>0";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
