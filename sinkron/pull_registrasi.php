<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://simeut.tirtaintan.co.id');

	// akses core garut
	include '../setDB05.php';
	// akses log
	include '../logging.php';
	$log    = new errorLog();
	
	$reg_id			= "";
	$reg_unit		= "";
	$reg_name		= "";
	$reg_address	= "";
	$reg_phone		= "";
	$reg_email		= "";
	$reg_identity	= "";
	$reg_tgl		= "";

	foreach (json_decode(base64_decode($_GET['data'])) as $key => $value) {
		$$key = $value;
	}

	define('_KODE', 'sinkron');
	define('_USER', 'auto_sync');
	define('_HOST', $_SERVER['REMOTE_ADDR']);
	define('_TOKN', uniqid());

	if(abs($reg_id)>0){
		try{
			$CLINK->beginTransaction();
			$que = "INSERT INTO pdam_gart.tm_pemohon_bp(pem_reg,kp_kode,dkd_kd,pem_nama,pem_alamat,pem_telp,pem_email,pem_ktp,pem_tgl_reg,pem_tgl_entry,usr_id,pem_sts) VALUES('".$reg_id."','".$reg_unit."',CONCAT('".$reg_unit."','0000'),'".$reg_name."','".$reg_address."','".$reg_phone."','".$reg_email."','".$reg_identity."','".$reg_tgl."',NOW(),'website',12)";
			$row = $CLINK->exec($que);
			if($row>0){
				$CLINK->commit();
				$pesan = "Data register: ".$reg_id." telah disimpan";
				$log->logDB($que);
			}
			else{
				$CLINK->rollback();
				$pesan = "Data register: ".$reg_id." tidak bisa disimpan";
			}
		}
		catch(PDOException $row){
			$CLINK->rollback();
			$pesan  = "Mungkin terjadi kesalahan pada koneksi database";
			$log->errorDB($row->getMessage());
			$log->logDB($que);
		}
	}
	else{
		$pesan 	= "Tidak ada data disinkron";
		$row	= array("pesan" => $pesan);
	}

	unset($CLINK);
	$log->logMess($pesan);
	echo json_encode($row);
    flush();
?>
