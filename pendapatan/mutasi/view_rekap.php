<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
    header('Access-Control-Allow-Origin: *');
	include $_SERVER['DOCUMENT_ROOT']."/api/setDB02.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	/* getParam **/

	/* database **/
	try {
        $que    = "SELECT IFNULL(a.rek_total,0) AS sambung_baru_total,IFNULL(b.rek_total,0) AS mutasi_golongan_total,CONCAT(SUBSTR(c.periode,1,4),'-',SUBSTR(c.periode,5,2)) AS periode FROM temp_gart.db_periode c LEFT JOIN (SELECT SUM(rek_total) AS rek_total,SUBSTR(rek_nomor,1,6) AS periode FROM temp_gart.sambung_baru_rekening GROUP BY SUBSTR(rek_nomor,1,6)) a ON(a.periode=c.periode) LEFT JOIN (SELECT SUM(selisih_uangair) AS rek_total,SUBSTR(rek_nomor,1,6) AS periode FROM temp_gart.mutasi_golongan_rekening GROUP BY SUBSTR(rek_nomor,1,6)) b ON(b.periode=c.periode) WHERE PERIOD_DIFF(DATE_FORMAT(NOW(),'%Y%m'),SUBSTR(c.periode,1,6))<30 ORDER BY c.periode";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
