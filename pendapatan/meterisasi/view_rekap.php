<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
    header('Access-Control-Allow-Origin: *');
	include $_SERVER['DOCUMENT_ROOT']."/api/setDB02.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	/* getParam **/

	/* database **/
	try {
        // $que    = "SELECT SUM(selisih_uangair) AS rek_total,CONCAT(SUBSTR(rek_nomor,1,4),'-',SUBSTR(rek_nomor,5,2)) AS periode FROM temp_gart.meterisasi_rekening WHERE PERIOD_DIFF(DATE_FORMAT(NOW(),'%Y%m'),SUBSTR(rek_nomor,1,6))<30 GROUP BY SUBSTR(rek_nomor,1,6)";
		$que    = "SELECT a.periode,a.rek_total AS pendapatan_total,b.rek_total AS meterisasi_total FROM (SELECT SUM(rek_total) AS rek_total,rek_bln,rek_thn,CONCAT(rek_thn,'-',REPEAT(0,2-LENGTH(rek_bln)),rek_bln) AS periode FROM info_gart.db_pendapatan GROUP BY rek_thn,rek_bln) a LEFT JOIN (SELECT SUM(selisih_uangair) AS rek_total,SUBSTR(rek_nomor,1,4) AS rek_thn,SUBSTR(rek_nomor,5,2) AS rek_bln FROM temp_gart.meterisasi_rekening GROUP BY SUBSTR(rek_nomor,1,6)) b ON(b.rek_bln=a.rek_bln AND b.rek_thn=a.rek_thn) WHERE PERIOD_DIFF(DATE_FORMAT(NOW(),'%Y%m'),CONCAT(a.rek_thn,REPEAT(0,2-LENGTH(a.rek_bln)),a.rek_bln))<30";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
