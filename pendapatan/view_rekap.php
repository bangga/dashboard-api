<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
    header('Access-Control-Allow-Origin: *');
	include $_SERVER['DOCUMENT_ROOT']."/api/setDB02.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	/* getParam **/

	/* database **/
	try {
        $que    = "SELECT SUM(rek_total) AS rek_total,CONCAT(rek_thn,'-',REPEAT(0,2-LENGTH(rek_bln)),rek_bln) AS periode FROM db_pendapatan WHERE PERIOD_DIFF(DATE_FORMAT(NOW(),'%Y%m'),CONCAT(rek_thn,REPEAT(0,2-LENGTH(rek_bln)),rek_bln))<30 GROUP BY rek_thn,rek_bln";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
